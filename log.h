#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <vulkan/vulkan.h>

#ifdef NDEBUG
#include <fstream>
#else
#include <iostream>
#endif // NDEBUG

#ifdef _WIN32

#ifdef BUILD
#define EXPORT __declspec(dllexport)
#else
#define EXPORT __declspec(dllimport)

#endif // BUILD

#else

#define EXPORT

#endif // _WIN32


namespace Log
{

enum class Severity
{
    INFO,
    WARNING,
    ERROR
};

#ifndef NDEBUG
EXPORT extern std::ostream & file;
#else
EXPORT extern std::ofstream file;
#endif // DEBUG

EXPORT extern void log(const char * msg,Severity severity = Severity::ERROR);
EXPORT extern VkBool32 debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT severity, VkDebugUtilsMessageTypeFlagsEXT type, const VkDebugUtilsMessengerCallbackDataEXT* data, void* userdata);

}

#endif // LOG_H_INCLUDED
