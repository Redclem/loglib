#include "log.h"

namespace Log
{

#ifndef NDEBUG
EXPORT std::ostream & file(std::cout);
#else
EXPORT std::ofstream file("log.txt");
#endif // NDEBUG

void log(const char * msg,Severity severity)
{
    switch(severity)
    {
    case Severity::ERROR:
        file << "[ERROR]:";
        break;
    case Severity::WARNING:
        file << "[WARNING]:";
        break;
    case Severity::INFO:
        file << "[INFO]:";
        break;
    }
    file << msg << std::endl;
}

VkBool32 debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT severity, VkDebugUtilsMessageTypeFlagsEXT, const VkDebugUtilsMessengerCallbackDataEXT * data, void *)
{
    file << "[LAYER";
    switch(severity)
    {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            file << " WARNING";
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
            file << " ERROR";
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
            file << " VERBOSE";
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
            file << " INFO";
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT:
            break;
    }
    file << "]:" << data->pMessage << std::endl;
    return VK_FALSE;
}

}
